<?php

/**
 * Created by PhpStorm.
 * User: Dobromir Ivanov
 * Date: 29.08.16
 * Description of Loader
 * @author Dobromir Ivanov
 */

namespace Fw;

use Command\ParseWebSiteCommand;
use Fw\Command\Finder;
use Fw\Console\ConsoleInterface;
use Fw\Console\Input;
use Fw\Console\Output;

include_once "Loader.php";


class Application {

	private static $instance = null;

	private $rootDir = null;
	/**
	 * @var $input Input
	 */
	private $input;
	/**
	 * @var $output Output
	 */
	private $output;

	/**
	 * @var $configuration Configuration
	 */
	private $configuration;

	/**
	 * @var $commands array
	 */
	private $commands = array();

	protected function __construct() {

		/**
		 * Register Loader
		 */
		set_exception_handler( array( $this, '_exceptionHandler' ) );
		\Fw\Loader::registerNamespace( 'Fw', dirname( __FILE__ ) . DIRECTORY_SEPARATOR );
		\Fw\Loader::registerNamespace( 'Command', dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '../Command' . DIRECTORY_SEPARATOR );
		\Fw\Loader::registerAutoLoad();
		$this->configuration = new Configuration();
	}


	public function run() {
		$this->input = new Input();
		$this->output = new Output();
		$this->input->parse();
		$this->execute();
	}

	/**
	 * @return Input
	 */
	public function getInput() {
		return $this->input;
	}

	/**
	 * @return Output
	 */
	public function getOutput() {
		return $this->output;
	}


	/**
	 * {@inheritdoc}
	 *
	 * @api
	 */
	public function getRootDir() {
		if ( null === $this->rootDir ) {
			$r             = new \ReflectionObject( $this );
			$this->rootDir = str_replace( '\\', '/', dirname( $r->getFileName() ) );
		}

		return $this->rootDir;
	}

	public function _exceptionHandler(\Throwable $exception ) {
		if($exception->getCode() == 0) {
			print_r($exception);
		}else{
			$this->output->printError($exception->getMessage());
		}

	}

	/**
	 * @return array
	 */
	public function registerCommands() {
		if ( ! $this->commands ) {

			$dir    = $this->configuration->get( 'command_folder' );
			$ns     = $this->configuration->get( 'command_ns' );
			$finder = new Finder();
			$p      = function ( \SplFileInfo $file ) {
				if ( preg_match( '/Command.php$/', $file->getFilename() ) ) {
					return true;
				}

				return false;
			};
			$files  = $finder->findSpecificFiles( $dir, $p );
			if ( $files ) {
				foreach ( $files as $file ) {
					/**
					 * @var $file \SplFileInfo
					 */
					$class = $ns . '\\' . $file->getBasename( '.php' );
					$ref   = new \ReflectionClass( $class );
					if ( $ref->implementsInterface( ConsoleInterface::class ) && ! $ref->isAbstract() ) {
						$this->commands[] = $ref->newInstance();
					}
				}
			}
		}

		return $this->commands;
	}

	/**
	 * @return Application
	 */
	public static function getInstance() {
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function execute() {
		$printMethods = [];
		foreach ( (array) $this->registerCommands() as $key => $registerCommand ) {

			/**
			 * @var $registerCommand ConsoleInterface
			 */
			if ( $registerCommand->getName() == $this->input->getMethod() ) {
				return $registerCommand->execute( $this->input, $this->output );
			}

			$printMethods[ $key ] = sprintf( 'Command: %s', $registerCommand->getName() );

			if ( method_exists( $registerCommand, '__toString' ) ) {
				$printMethods[ $key ] .= sprintf( '%s ---- Description %s', PHP_EOL, $registerCommand->__toString() );
			}
		}

		return $this->output->printSuccess( implode( PHP_EOL, $printMethods ) );
	}

	/**
	 * @return Configuration
	 */
	public function getConfiguration() {
		return $this->configuration;
	}
}