<?php

/**
 * Created by PhpStorm.
 * User: doba
 * Date: 29.08.16
 * Time: 10:24
 */
namespace Fw\Console;


interface ConsoleInterface {

	public function execute(Input $input,Output $output);

	public function getName();

}