<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 29.08.16
 * Time: 10:40
 */

namespace Fw\Console;


use Fw\Application;

abstract class BaseConsole {


	/**
	 * @return Application
	 */
	protected function getApplication(){

		return Application::getInstance();
	}
}