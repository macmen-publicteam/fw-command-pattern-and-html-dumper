<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 31.08.16
 * Time: 17:49
 */

namespace Fw\Http\Client\Exception;


use Exception;

class LogicException extends \LogicException {

	public function __construct( $message, $code = 500, Exception $previous = null) {
		parent::__construct( $message, $code, $previous );
	}


}