<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 31.08.16
 * Time: 13:40
 */

namespace Fw\Http\Client;


class Request {

    /**
     * Gets header only
     *
     * @var bool
     */
    private $nobody = false;

    /**
     * null|string - "socket" or "curl", null to use default.
     *
     * @var null
     */
    private $handler = null;

    /**
     * @var float
     */
    private $protocolVersion = 1.1;

    private $body = null;

    private $query = array();

    static $userAgents = [
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.53.11 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10',
        'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:11.0) Gecko/20100101 Firefox/11.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/534.52.7 (KHTML, like Gecko) Version/5.1.2 Safari/534.52.7',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7',
        'Mozilla/5.0 (Windows NT 5.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.52.7 (KHTML, like Gecko) Version/5.1.2 Safari/534.52.7',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.79 Safari/535.11',
        'Mozilla/5.0 (Windows NT 5.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.83 Safari/535.11',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
        'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.54.16 (KHTML, like Gecko) Version/5.1.4 Safari/534.54.16',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:10.0.2) Gecko/20100101 Firefox/10.0.2',
        'Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko/20100101 Firefox/11.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
        'Mozilla/5.0 (Windows NT 6.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1',
        'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0) Gecko/20100101 Firefox/10.0',
        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
        'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.79 Safari/535.11',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:9.0.1) Gecko/20100101 Firefox/9.0.1',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:11.0) Gecko/20100101 Firefox/11.0',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7',
        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:10.0.2) Gecko/20100101 Firefox/10.0.2',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)',
        'Mozilla/5.0 (iPad; CPU OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.1) Gecko/20100101 Firefox/10.0.1',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:9.0.1) Gecko/20100101 Firefox/9.0.1',
        'Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.83 Safari/535.11',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.79 Safari/535.11',
        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7',
        'Opera/9.64(Windows NT 5.1; U; en) Presto/2.1.1',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7',
        'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; Media Center PC 6.0; InfoPath.2; MS-RTC LM 8)'
    ];

    /**
     * @var $method string
     * GET,POST,DELETE,PATH
     */
    private $method;

    /**
     * @var array
     */
    private $headers = array();

    /**
     * @var $uri string
     */
    private $uri;

    /**
     * @var array
     */
    private $parseUri = array();

    /**
     * Request constructor.
     *
     * @param        $uri
     * @param string $method
     */
    public function __construct( $uri, $method = 'GET') {
        $this->setMethod( $method );
        $this->uri = $uri;
        $this->parseUri = $this->parseUri($uri);

        $this->addHeader( 'Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' )
             ->addHeader( 'Accept-Encoding', 'gzip, deflate, sdch' )
             ->addHeader( 'Accept-Language', 'en-US,en;q=0.8,bg;q=0.6' )
             ->addHeader( 'Cache-Control', 'max-age=0' )
             ->addHeader( 'Connection', 'keep-alive' )
             ->addHeader( 'Host', $this->parseUri['host'] )
             ->addHeader( 'Upgrade-Insecure-Requests', 1 )
             ->addHeader( 'User-Agent', self::getRandomUserAgent() );
    }


    public function setMethod( $method ) {
        $this->method = strtoupper( $method );

        return $this;
    }

    public function addHeader( $key, $value ) {
        $this->headers[ $key ] = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders() {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getMethod() {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public static function getRandomUserAgent() {
        return array_rand( self::$userAgents );
    }

    /**
     * @return string
     */
    public function getUri() {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return $this;
     */
    public function setUri( $uri ) {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNobody() {
        return $this->nobody;
    }

    /**
     * @param mixed $nobody
     *
     * @return $this;
     */
    public function setNobody( $nobody ) {
        $this->nobody = $nobody;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * @param string $body
     *
     * @return $this;
     */
    public function setBody( $body ) {
        $this->body = $body;

        return $this;
    }

    /**
     * @return float
     */
    public function getProtocolVersion() {
        return $this->protocolVersion;
    }

    /**
     * @param float $protocolVersion
     *
     * @return $this;
     */
    public function setProtocolVersion( $protocolVersion ) {
        $this->protocolVersion = $protocolVersion;

        return $this;
    }

    /**
     * @return null
     */
    public function getHandler() {
        return $this->handler;
    }

    /**
     * @param null $handler
     *
     * @return $this;
     */
    public function setHandler( $handler ) {
        $this->handler = $handler;

        return $this;
    }

    /**
     * @param      $name
     * @param null $default
     *
     * @return mixed
     */
    public function getQuery( $name, $default = null ) {
        return isset( $this->query[ $name ] ) ? $this->query[ $name ] : $default;
    }

    /**
     * @param $key
     * @param $value
     *
     * @return $this ;
     * @internal param array $query
     *
     */
    public function setQuery( $key, $value ) {
        $this->query[ $key ] = $value;

        return $this;
    }


    /**
     * Parse request uri.
     *
     * @param string $uri
     *
     * @return array
     */
    protected function parseUri( $uri ) {
        $parts    = parse_url( $uri );
        $scheme   = isset( $parts['scheme'] ) ? $parts['scheme'] : '';
        $user     = isset( $parts['user'] ) ? $parts['user'] : '';
        $pass     = isset( $parts['pass'] ) ? $parts['pass'] : '';
        $host     = isset( $parts['host'] ) ? $parts['host'] : '';
        $port     = isset( $parts['port'] ) ? $parts['port'] : null;
        $path     = isset( $parts['path'] ) ? $parts['path'] : '/';
        $query    = isset( $parts['query'] ) ? $parts['query'] : '';
        $fragment = isset( $parts['fragment'] ) ? $parts['fragment'] : '';

        return compact( 'scheme', 'user', 'pass', 'host', 'port', 'path', 'query', 'fragment' );
    }

    /**
     * @return array
     */
    public function getParseUri() {
        return $this->parseUri;
    }

    /**
     * @param array $parseUri
     *
     * @return $this;
     */
    public function setParseUri( $parseUri ) {
        $this->parseUri = $parseUri;

        return $this;
    }

}