<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 31.08.16
 * Time: 14:04
 */

namespace Fw\Http\Client;


class Response {


	/**
	 * Status codes translation table.
	 *
	 * The list of codes is complete according to the
	 * {@link http://www.iana.org/assignments/http-status-codes/ Hypertext Transfer Protocol (HTTP) Status Code Registry}
	 * (last updated 2016-03-01).
	 *
	 * Unless otherwise noted, the status code is defined in RFC2616.
	 *
	 * @var array
	 */
	public static $statusTexts = array(
		100 => 'Continue',
		101 => 'Switching Protocols',
		102 => 'Processing',            // RFC2518
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		207 => 'Multi-Status',          // RFC4918
		208 => 'Already Reported',      // RFC5842
		226 => 'IM Used',               // RFC3229
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		307 => 'Temporary Redirect',
		308 => 'Permanent Redirect',    // RFC7238
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Payload Too Large',
		414 => 'URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Range Not Satisfiable',
		417 => 'Expectation Failed',
		418 => 'I\'m a teapot',                                               // RFC2324
		421 => 'Misdirected Request',                                         // RFC7540
		422 => 'Unprocessable Entity',                                        // RFC4918
		423 => 'Locked',                                                      // RFC4918
		424 => 'Failed Dependency',                                           // RFC4918
		425 => 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
		426 => 'Upgrade Required',                                            // RFC2817
		428 => 'Precondition Required',                                       // RFC6585
		429 => 'Too Many Requests',                                           // RFC6585
		431 => 'Request Header Fields Too Large',                             // RFC6585
		451 => 'Unavailable For Legal Reasons',                               // RFC7725
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported',
		506 => 'Variant Also Negotiates (Experimental)',                      // RFC2295
		507 => 'Insufficient Storage',                                        // RFC4918
		508 => 'Loop Detected',                                               // RFC5842
		510 => 'Not Extended',                                                // RFC2774
		511 => 'Network Authentication Required',                             // RFC6585
	);

	/**
	 * @var array
	 */
	private $headers = array();

	/**
	 * @var int
	 */
	private $status;

	/**
	 * @var string
	 */
	private $statusText;

	/**
	 * @var float
	 */
	private $protocolVersion;

	/**
	 * @var string
	 */
	private $body = null;

	public function __construct( $body, $headers ) {
		$this->body = $body;
		$this->setProtocolVersion( '1.0' );
		$this->setStatus( 200 );
		$this->parseResponseHeaders( $headers );
	}


	/**
	 * @return string
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * @param string $body
	 *
	 * @return $this;
	 */
	public function setBody( $body ) {
		$this->body = $body;

		return $this;
	}

	protected function parseResponseHeaders( $headers ) {
		$slitHeader = explode( "\r\n", $headers );
		$lines      = array_filter( $slitHeader );
		preg_match( '#^HTTP/([\d\.]+)\s(\d+)\s(.*?)$#i', array_shift( $lines ), $match );

		if ( isset( $match[1] ) ) {
			$this->setProtocolVersion( $match[1] );
		}
		if ( isset( $match[2] ) ) {
			$status = (int) $match[2];
			$this->setStatus( $status );
		}
		$this->addHeaderToArray( $this->headers, $lines, null );
	}


	/**
	 * Used for adding headers to an array.
	 *
	 * @param array           &$builder
	 * @param string|array    $name
	 * @param string|string[] $values
	 * @param boolean         $append
	 */
	private function addHeaderToArray( &$builder, $name, $values, $append = true ) {
		if ( is_array( $name ) ) {
			foreach ( $name as $key => $value ) {
				if ( is_int( $key ) ) {
					list( $key, $value ) = array_map( 'trim', explode( ':', $value, 2 ) );
				}
				$this->addHeaderToArray( $builder, $key, $value, $append );
			}

			return;
		}
		$normalizedKey = $this->normalizeHeaderKey( $name );

		if ( ! $append || ! isset( $builder[ $normalizedKey ] ) ) {
			$builder[ $normalizedKey ] = array();
		}

		foreach ( (array) $values as $value ) {
			if ( ! is_string( $value ) && ! is_numeric( $value ) ) {
				throw new \InvalidArgumentException( 'Header value must be a string or array of string.' );
			}
			$builder[ $normalizedKey ][] = array(
				'key'   => $name,
				'value' => trim( $value )
			);
		}
	}

	/**
	 * Normalize case-insensitive key.
	 *
	 * @param string $key
	 *
	 * @return string
	 */
	private function normalizeHeaderKey( $key ) {
		return strtr( strtolower( $key ), '_', '-' );
	}

	/**
	 * @return int
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @param int $status
	 *
	 * @return $this;
	 */
	public function setStatus( $status ) {
		$this->status = $status;

		$statusText = $this->getStatusTextByCode( $status );

		$this->setStatusText( $statusText );

		return $this;
	}

	/**
	 * @return float
	 */
	public function getProtocolVersion() {
		return $this->protocolVersion;
	}

	/**
	 * @param float $protocolVersion
	 *
	 * @return $this;
	 */
	public function setProtocolVersion( $protocolVersion ) {
		$this->protocolVersion = $protocolVersion;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getStatusText() {
		return $this->statusText;
	}


	/**
	 * Is response successful?
	 *
	 * @return bool
	 */
	public function isSuccessful() {
		return $this->status >= 200 && $this->status < 300;
	}

	/**
	 * Is the response a redirect?
	 *
	 * @return bool
	 */
	public function isRedirection() {
		return $this->status >= 300 && $this->status < 400;
	}

	/**
	 * Is there a client error?
	 *
	 * @return bool
	 */
	public function isClientError() {
		return $this->status >= 400 && $this->status < 500;
	}

	/**
	 * @return bool
	 */
	public function isOk() {
		return $this->status == 200;
	}


	/**
	 * @param string $statusText
	 *
	 * @return $this;
	 */
	public function setStatusText( $statusText ) {
		$this->statusText = $statusText;

		return $this;
	}

	protected function getStatusTextByCode( $status ) {
		if ( array_key_exists( $status, self::$statusTexts ) ) {
			return self::$statusTexts[ $status ];
		}
		throw new \InvalidArgumentException( sprintf( 'The Status %s is not valid', $status ) );
	}

	/**
	 * @return \DOMDocument
	 */
	public function getBodyAsDomDocument() {
		return $this->getDomDocument( $this->getBody() );
	}


	/**
	 * Adds HTML/XML content.
	 *
	 * If the charset is not set via the content type, it is assumed
	 * to be ISO-8859-1, which is the default charset defined by the
	 * HTTP 1.1 specification.
	 *
	 * @param string      $content A string to parse as HTML/XML
	 * @param null|string $type The content type of the string
	 *
	 * @return \DOMDocument|void
	 */
	public function getDomDocument( $content, $type = null ) {
		if ( empty( $type ) ) {
			$type = 0 === strpos( $content, '<?xml' ) ? 'application/xml' : 'text/html';
		}

		// DOM only for HTML/XML content
		if ( ! preg_match( '/(x|ht)ml/i', $type, $xmlMatches ) ) {
			return;
		}

		$charset = null;
		if ( false !== $pos = stripos( $type, 'charset=' ) ) {
			$charset = substr( $type, $pos + 8 );
			if ( false !== $pos = strpos( $charset, ';' ) ) {
				$charset = substr( $charset, 0, $pos );
			}
		}
		if ( null === $charset &&
		     preg_match( '/\<meta[^\>]+charset *= *["\']?([a-zA-Z\-0-9_:.]+)/i', $content, $matches )
		) {
			$charset = $matches[1];
		}
		if ( null === $charset ) {
			$charset = 'ISO-8859-1';
		}

		return $this->addHtmlContent( $content, $charset );
	}


	/**
	 * @param        $content
	 * @param string $charset
	 *
	 * @return \DOMDocument
	 */
	public function addHtmlContent( $content, $charset = 'UTF-8' ) {
		$internalErrors  = libxml_use_internal_errors( true );
		$disableEntities = libxml_disable_entity_loader( true );

		$dom                  = new \DOMDocument( '1.0', $charset );
		$dom->validateOnParse = true;

		set_error_handler( function () {
			throw new \Exception();
		} );

		try {
			// Convert charset to HTML-entities to work around bugs in DOMDocument::loadHTML()
			$content = mb_convert_encoding( $content, 'HTML-ENTITIES', $charset );
		} catch ( \Exception $e ) {
		}
		restore_error_handler();
		if ( '' !== trim( $content ) ) {
			@$dom->loadHTML( $content );
		}
		libxml_use_internal_errors( $internalErrors );
		libxml_disable_entity_loader( $disableEntities );

		return $dom;
	}

}