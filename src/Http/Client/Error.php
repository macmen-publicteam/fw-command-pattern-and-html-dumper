<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 31.08.16
 * Time: 10:04
 */

namespace Fw\Http\Client;


class Error {


	private $timeStart = null;    // null|float time start sending
	private $timeProcess = null;    // null|float time for execute sending request
	private $handler = null;
	private $errors = array();

	/**
	 * @return array
	 */
	public function getErrors() {
		return $this->errors;
	}

	/**
	 * @param array $errors
	 *
	 * @return $this;
	 */
	public function setErrors( array $errors ) {
		$this->errors = $errors;

		return $this;
	}

	/**
	 * @param $error
	 *
	 * @return $this
	 */
	public function addError( $error ) {
		$this->errors[] = $error;
		return $this;
	}

	/**
	 * @return null
	 */
	public function getTimeStart() {
		return $this->timeStart;
	}

	/**
	 * @param null $timeStart
	 *
	 * @return $this;
	 */
	public function setTimeStart( $timeStart ) {
		$this->timeStart = $timeStart;

		return $this;
	}

	/**
	 * @return null
	 */
	public function getHandler() {
		return $this->handler;
	}

	/**
	 * @param null $handler
	 *
	 * @return $this;
	 */
	public function setHandler( $handler ) {
		$this->handler = $handler;

		return $this;
	}

	/**
	 * @return null
	 */
	public function getTimeProcess() {
		return $this->timeProcess;
	}

	/**
	 * @param null $timeProcess
	 *
	 * @return $this;
	 */
	public function setTimeProcess( $timeProcess ) {
		$this->timeProcess = $timeProcess;

		return $this;
	}


}