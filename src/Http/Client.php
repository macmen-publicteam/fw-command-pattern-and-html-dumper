<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 31.08.16
 * Time: 10:04
 */

namespace Fw\Http;


use Fw\Http\Client\Exception\LogicException;
use Fw\Http\Client\Request;
use Fw\Http\Client\Response;

class Client {

	/**
	 * @var int
	 */
	private $timeout = 10;

	/**
	 * @param        $uri
	 * @param string $method
	 *
	 * @return bool|Response
	 */
	public function request( $uri, $method = "GET" ) {
		$request = new Request( $uri, $method );

		return $this->requestWithCurl( $request );
	}


	/**
	 * @param Request $request
	 *
	 * @return bool|Response
	 */
	protected function requestWithCurl( Request $request ) {
		$curlOptions = array(
			CURLOPT_CUSTOMREQUEST  => $request->getMethod(),
			CURLOPT_URL            => $request->getUri(),
			CURLOPT_HEADER         => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_ENCODING       => 'gzip, deflate',
			CURLOPT_NOBODY         => $request->getNobody(),
			CURLOPT_TIMEOUT        => $this->timeout,
			CURLOPT_HTTPHEADER     => $request->getHeaders(),
		);

		if ( $request->getProtocolVersion() === '1.0' ) {
			$curlOptions[ CURLOPT_HTTP_VERSION ] = CURL_HTTP_VERSION_1_0;
		} else {
			$curlOptions[ CURLOPT_HTTP_VERSION ] = CURL_HTTP_VERSION_1_1;
		}

		if ( $body = $request->getBody() ) {
			$curlOptions[ CURLOPT_POSTFIELDS ] = $body;
		}


		$ch = curl_init();
		curl_setopt_array( $ch, $curlOptions );
		$response = curl_exec( $ch );

		if ( $response === false ) {
			throw new LogicException( sprintf( 'ERROR: %d - %s.', curl_errno( $ch ), curl_error( $ch ) ) );
		}

		$headerSize = curl_getinfo( $ch, CURLINFO_HEADER_SIZE );
		curl_close( $ch );

		$headers = (string) substr( $response, 0, $headerSize );
		$body    = (string) substr( $response, $headerSize );


		return new Response( $body, $headers );
	}


	/**
	 * @param int $maxRedirection
	 *
	 * @return $this;
	 */
	public function setMaxRedirection( $maxRedirection ) {
		$this->maxRedirection = $maxRedirection;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getTimeout() {
		return $this->timeout;
	}

	/**
	 * @param int $timeout
	 *
	 * @return $this;
	 */
	public function setTimeout( $timeout ) {
		$this->timeout = $timeout;

		return $this;
	}

}