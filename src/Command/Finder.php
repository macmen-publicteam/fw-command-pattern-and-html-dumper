<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 29.08.16
 * Time: 11:01
 */

namespace Fw\Command;


class Finder {


	/**
	 * @param $dir
	 *
	 * @return array
	 */
	public function in( $dir ) {
		$result = array();
		if ( ! is_dir( $dir ) ) {
			throw new \InvalidArgumentException( sprintf( 'The "%s" directory does not exist.', $dir ) );
		}
		$files = array_diff( scandir( $dir ), array( '.', '..' ) );
		if ( count( $files ) > 0 ) {
			foreach ( $files as $file ) {
				$result[] = new \SplFileInfo( $dir . DIRECTORY_SEPARATOR . $file );
			}
		}

		return $result;
	}


	public function findSpecificFiles($dir,\Closure $closure){
		$files = $this->in($dir);
		$result = array();
		if($files){
			foreach ( $files as $key => $file ) {
				if($closure($file,$key)) {
					$result[$key] = $file;
				}
			}
		}
		return $result;
	}
}