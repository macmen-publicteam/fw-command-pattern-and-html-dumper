<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 30.08.16
 * Time: 16:46
 */

namespace Fw\Exception;


use Exception;

class RequiredCommandParameterException extends \Exception {
	public function __construct( $message, $code = 500, Exception $previous =null ) {
		$code = 500;
		parent::__construct( $message, $code, $previous );
	}


}