<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 31.08.16
 * Time: 17:53
 */

namespace Fw;


class WebPage {

    /**
     * @var $document \DOMDocument
     */
    private $document;

    /**
     * WebPage constructor.
     *
     * @param \DOMDocument $document
     */
    public function __construct( \DOMDocument $document ) {
        $this->document = $document;
    }

    /**
     * @return null|string
     */
    public function getTitle() {
        $dom = $this->document->getElementsByTagName( 'title' );
        if ( $dom->length > 0 ) {
            return $dom->item( 0 )->textContent;
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getDescription() {
        $dom = $this->document->getElementsByTagName( 'meta' );
        if ( $dom->length > 0 ) {
            foreach ( $dom as $item ) {
                /**
                 * @var $item \DOMElement
                 */
                if ( trim( strtolower( $item->getAttribute( 'name' ) ) ) == 'description' ) {
                    return $item->getAttribute( 'content' );
                }
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getHeaderTags() {
        $elements = array();
        for ( $i = 0; $i <= 6; $i ++ ) {
            $element    = sprintf( 'h%s', $i );
            $domElement = $this->document->getElementsByTagName( $element );
            if ( $domElement->length > 0 ) {
                foreach ( $domElement as $item ) {
                    $elements[ $element ][] = $item->textContent;
                }
            }
        }

        return $elements;
    }


    /**
     * @return \DOMDocument
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * @param \DOMDocument $document
     *
     * @return $this;
     */
    public function setDocument( \DOMDocument $document ) {
        $this->document = $document;

        return $this;
    }

}