<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 29.08.16
 * Time: 11:13
 */

namespace Fw;


use Fw\Exception\InvalidConfigurationException;

final class Configuration {

	/**
	 * @var array
	 */
	private $config = array();


	/**
	 * @param $key
	 * @param $value
	 *
	 * @return $this ;
	 */
	public function setConfig( $key, $value ) {
		$this->config[ $key ] = $value;

		return $this;
	}

	/**
	 * @param $key
	 * @param $value
	 *
	 * @return Configuration
	 */
	public function add( $key, $value ) {
		return $this->setConfig( $key, $value );
	}

	/**
	 * @param $name
	 *
	 * @return bool
	 */
	public function isExist( $name ) {
		if ( array_key_exists( $name, $this->config ) ) {
			return true;
		}

		return false;
	}

	/**
	 * @param $name
	 *
	 * @return mixed
	 * @throws InvalidConfigurationException
	 */
	public function get( $name ) {
		if ( $this->isExist( $name ) ) {
			return $this->config[ $name ];
		}
		throw new InvalidConfigurationException( sprintf( 'Configuration with name: %s not found', $name ) );
	}

	/**
	 * @return array
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * @return array
	 */
	public function all(){
		return $this->getConfig();
	}

}