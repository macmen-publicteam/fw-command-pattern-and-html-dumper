<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 29.08.16
 * Time: 12:51
 */

namespace Command;


use Fw\Console\BaseConsole;
use Fw\Console\ConsoleInterface;
use Fw\Console\Input;
use Fw\Console\Output;
use Fw\Exception\RequiredCommandParameterException;
use Fw\Http\Client\Exception\InvalidResponseException;
use Fw\Http\Client;
use Fw\WebPage;

class ParseWebSiteCommand extends BaseConsole implements ConsoleInterface {

	public function execute( Input $input, Output $output ) {
		$url = $input->get( 'url' );
		if ( ! $url || ! filter_var( $url, FILTER_VALIDATE_URL ) ) {
			throw new RequiredCommandParameterException( 'The parameter "--url" is required or value is not valid' );
		}
		$output->printInfo( sprintf( 'parse url: %s', $url ) );
		$client   = new Client();
		$response = $client->request( $url );

		if ( ! $response ) {
			throw new InvalidResponseException( 'Missing Content or the page is broken' );
		}

		if ( ! $response->isSuccessful() ) {
			throw new InvalidResponseException(sprintf('Have a problem with the page. Status: %s Code: %s',$response->getStatus(),$response->getStatusText()));
		}



		$webPage = new WebPage( $response->getBodyAsDomDocument() );
		$output->printSuccess( sprintf( '--- Title: %s', $webPage->getTitle() ) );
		$output->printSuccess( sprintf( '--- Description: %s', $webPage->getDescription() ) );
		$hTags = $webPage->getHeaderTags();
		if(count($hTags) > 0) {
			foreach ( $hTags as $h => $hTag ) {
				$output->printSuccess(sprintf('------ %s ------',$h));
				$output->printSuccess(sprintf('%s',implode(PHP_EOL,$hTag)));
				$output->printSuccess('------------');
			}
		}
		$output->printInfo('Done!');
	}

	public function getName() {
		return 'parse:html';
	}

	function __toString() {
		return 'Parse web page and get Title description and H1,H2,H3';
	}


}